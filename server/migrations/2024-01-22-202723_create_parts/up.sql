-- Your SQL goes here
-- Your SQL goes here
CREATE TABLE IF NOT EXISTS parts 
(
    id INTEGER PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    description TEXT NOT NULL
);