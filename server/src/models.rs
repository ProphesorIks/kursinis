use crate::schema::*;
use diesel::prelude::*;
use rocket::serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize, Serialize, Queryable, Insertable)]
#[serde(crate = "rocket::serde")]
#[diesel(table_name = parts)]
pub struct Part {
    pub id: i32,
    pub name: String,
    pub description: String,
}

#[derive(Debug, Clone, Serialize, Deserialize, Insertable, Queryable)]
#[serde(crate = "rocket::serde")]
#[diesel(table_name = parts)]
pub struct NewPart {
    pub name: String,
    pub description: String,
}
