use crate::diesel_sqlite::*;
use crate::models::*;
use crate::schema::*;
use diesel::prelude::*;
use diesel::RunQueryDsl;
use rocket::response::status::Created;
use rocket::serde::json::Json;

#[post("/", data = "<new_part>")]
pub async fn create(db: Db, new_part: Json<NewPart>) -> Result<Created<Json<i32>>> {
    let id: i32 = db
        .run(move |conn| {
            diesel::insert_into(parts::table)
                .values(&*new_part)
                .returning(parts::id)
                .get_result(conn)
        })
        .await?;

    Ok(Created::new("/").body(Json(id)))
}

#[get("/")]
pub async fn list(db: Db) -> Result<Json<Vec<i32>>> {
    let ids: Vec<i32> = db
        .run(move |conn| parts::table.select(parts::id).load(conn))
        .await?;
    Ok(Json(ids))
}

#[get("/page/<page>", data = "<pagination>")]
pub async fn page(
    db: Db,
    page: i64,
    pagination: Option<Json<Pagination>>,
) -> Result<Json<Vec<Part>>> {
    let pagination: Pagination = if let Some(p) = pagination {
        p.0.to_clamped()
    } else {
        Pagination::default()
    };

    let parts: Vec<Part> = db
        .run(move |conn| {
            parts::table
                .select(parts::all_columns)
                .offset((page * pagination.per_page) + pagination.offset)
                .limit(pagination.per_page)
                .load(conn)
        })
        .await?;
    Ok(Json(parts))
}

#[get("/page")]
pub async fn page_head(db: Db) -> Result<Json<i64>> {
    let part_count: i64 = db
        .run(move |conn| parts::table.count().get_result(conn))
        .await?;

    Ok(Json(part_count))
}

#[get("/<id>")]
pub async fn read(db: Db, id: i32) -> Option<Json<Part>> {
    db.run(move |conn| parts::table.filter(parts::id.eq(id)).first(conn))
        .await
        .map(Json)
        .ok()
}

#[post("/patch/<id>", data = "<part>")]
pub async fn patch(db: Db, id: i32, part: Json<NewPart>) -> Result<Option<Json<()>>> {
    let affected = db
        .run(move |conn| {
            diesel::update(parts::table.filter(parts::id.eq(id)))
                .set((
                    parts::name.eq(part.name.clone()),
                    parts::description.eq(part.description.clone()),
                ))
                .execute(conn)
        })
        .await?;

    Ok((affected == 1).then(|| Json(())))
}

#[patch("/<id>", data = "<part>")]
pub async fn patch_patch(db: Db, id: i32, part: Json<NewPart>) -> Result<Option<Json<()>>> {
    let affected = db
        .run(move |conn| {
            diesel::update(parts::table.filter(parts::id.eq(id)))
                .set((
                    parts::name.eq(part.name.clone()),
                    parts::description.eq(part.description.clone()),
                ))
                .execute(conn)
        })
        .await?;

    Ok((affected == 1).then(|| Json(())))
}

#[delete("/<id>")]
pub async fn delete(db: Db, id: i32) -> Result<Option<()>> {
    let affected = db
        .run(move |conn| {
            diesel::delete(parts::table)
                .filter(parts::id.eq(id))
                .execute(conn)
        })
        .await?;

    Ok((affected == 1).then(|| ()))
}
