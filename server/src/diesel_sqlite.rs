use std::default;

use crate::routes::parts::*;
use rocket::fairing::AdHoc;
use rocket::response::{status::Created, Debug};
use rocket::serde::{Deserialize, Serialize};
use rocket::{serde, Build, Rocket};

#[database("diesel")]
pub struct Db(diesel::SqliteConnection);

pub type Result<T, E = Debug<diesel::result::Error>> = std::result::Result<T, E>;

async fn run_migrations(rocket: Rocket<Build>) -> Rocket<Build> {
    use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};

    const MIGRATIONS: EmbeddedMigrations = embed_migrations!("./migrations");
    Db::get_one(&rocket)
        .await
        .expect("database connection")
        .run(|conn| {
            conn.run_pending_migrations(MIGRATIONS)
                .expect("diesel migrations");
        })
        .await;

    rocket
}

pub fn stage() -> AdHoc {
    AdHoc::on_ignite("Diesel SQLite Stage", |rocket| async {
        rocket
            .attach(Db::fairing())
            .attach(AdHoc::on_ignite("Diesel Migrations", run_migrations))
            .mount(
                "/parts",
                routes![
                    list,
                    read,
                    create,
                    delete,
                    page,
                    patch,
                    patch_patch,
                    page_head
                ],
            )
    })
}

const MAX_PER_PAGE: i64 = 20;

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct Pagination {
    pub per_page: i64,
    pub offset: i64,
}

impl Default for Pagination {
    fn default() -> Self {
        Pagination {
            per_page: MAX_PER_PAGE,
            offset: 0,
        }
    }
}

impl Pagination {
    pub fn to_clamped(&self) -> Self {
        Pagination {
            per_page: self.per_page.min(MAX_PER_PAGE),
            offset: self.offset,
        }
    }
}
