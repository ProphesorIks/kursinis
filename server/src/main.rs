use std::error::Error;

use rocket::{
    fairing::{Fairing, Info, Kind},
    http::{Header, Method},
    Request, Response,
};
use rocket_cors::{AllowedHeaders, AllowedOrigins};

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_sync_db_pools;

mod diesel_sqlite;
mod models;
mod routes;
mod schema;

#[rocket::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let allowed_origins = AllowedOrigins::All;
    let cors = rocket_cors::CorsOptions {
        allowed_origins,
        allowed_methods: vec![
            Method::Get,
            Method::Put,
            Method::Patch,
            Method::Delete,
            Method::Head,
        ]
        .into_iter()
        .map(From::from)
        .collect(),
        allowed_headers: AllowedHeaders::some(&["Authorization", "Accept"]),
        allow_credentials: true,
        ..Default::default()
    }
    .to_cors()?;

    rocket::build()
        .attach(cors)
        .attach(diesel_sqlite::stage())
        .launch()
        .await?;

    Ok(())
}
