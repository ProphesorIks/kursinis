// @generated automatically by Diesel CLI.

diesel::table! {
    parts (id) {
        id -> Integer,
        name -> Text,
        description -> Text,
    }
}
