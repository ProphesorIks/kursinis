export type Listing = {
  id: number,
  partReference: number,
  storeName: string,
}