import { Result, Ok, Err } from '@ts-std/monads'

export type ServerError = {
  error: string,
  status: number
}
export type FetchResult<T> = Result<T, ServerError>

export const serverFetch = async (endpoint: string, data: RequestInit | undefined) => {
  const url = "http://127.0.0.1:8000/"
  return fetch(`${url}${endpoint}`, data)
}

export const fetchResult = async <T> (endpoint: string, data: RequestInit | undefined): Promise<FetchResult<T>> => {
  let r: Response;
  try {
    r = await serverFetch(endpoint, data);
    if(!r.ok) {
      return Err({
        error: r.statusText,
        status: r.status
      })
    }
  }catch(e) {
    return Err({
      error: `Server error: ${e}`,
      status: 500
    })
  }

  try {
    let json = await r.json();
    return Ok(json as T);
  }catch(e) {
    return Err({
      error: `Json decode error: ${e}`,
      status: r.status 
    })
  }
}